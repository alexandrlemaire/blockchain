import hashlib

def start_par_x0(difficulte, message)->bool:
    for i in range (0,difficulte):
        if message[i] != '0':
            return False
    return True

def proof_of_work(difficulte:int, transactions:str, last_hash:str)->str:
    hash=""
    while 1:
        nonce = gen_nonce(64)
        block = transactions+last_hash+nonce
        block = bytes(block,'utf-8')
        hash = hashlib.sha256(block).hexdigest()
        if start_par_x0(difficulte, hash):
            break
    print('Le nonce est : '+ nonce)
    print ('Le hash est : '+ hash)


proof_of_work(4, 'transac', 'lhash')