import random
import string


def fonct_lignes(nb_lignes):
    for i in range(0, nb_lignes):
        print("Voici un nombre aléatoire : "+str(random.randstr(a,z)))


def gen_lettres():
        return random.choice(string.ascii_letters)

def gen_chiffre():
    return random.randint(0,9)

def gen_chiffres_lettres():
    return str(gen_chiffre()) + gen_lettres()

def gen_nonce(x):
    """Renvoie un nonce de 8 caractères"""
    nonce = ""
    for i in range(x):
        hasard = random.randint(0,1)
        if hasard ==0:
            nonce = nonce + str(gen_chiffre())
        else:
            nonce = nonce + gen_lettres()
    return nonce


print(gen_nonce(10))